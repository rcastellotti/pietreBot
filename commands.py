import logging
import os
from telegram import ParseMode, ChatMemberOwner
from telegram.chatmember import ChatMemberAdministrator
from telegram import Bot, User
from telegram.utils.helpers import escape_markdown
import re

TELEGRAM_TOKEN = os.environ["TELEGRAM_TOKEN"]
LAST_VIDEO_URL= "https://www.youtube.com/playlist?list=UUO6Q4WydyXJMnRPWQr2cB2g&playnext=1&index=1"
bot = Bot(TELEGRAM_TOKEN)


def admin(func):
    def wrapper(*args, **kwargs):
        update = args[0]
        user = (bot.get_chat_member(
            update.message.chat.id, update.message.from_user.id))
        if isinstance(user, (ChatMemberAdministrator, ChatMemberOwner)):
            func(*args, **kwargs)
        else:
            msg = "*errore:* devi essere un admin per usare questo comando"
            update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN_V2)
    return wrapper


def help_command(update, context):
    msg = ("comandi disponibili:\n"
           "/ping \~\> monitor status\n"
           "/polonia `<RIDER>` \~\> copypasta di [Per chi tira la Polonia](https://www.youtube.com/watch?v=hWBKHhksGXE) di Conti\n"
           "/help \~\> invia questo messaggio; \n\n"
           "misc: /heymoto, /debotto"
           "\n\nsource code: [https://gitlab\.com/rcastellotti/pietreBot](https://gitlab\.com/rcastellotti/pietreBot)")
    update.message.reply_text(
        msg, parse_mode=ParseMode.MARKDOWN_V2, disable_web_page_preview=True)


@admin
def ban(update, context):
    try:
        id_to_ban = update["message"]["reply_to_message"]["new_chat_members"][0]["id"]
    except:
        id_to_ban = update["message"]["reply_to_message"]["from_user"]["id"]

    chat_id = update["message"]["chat"]["id"]
    bot.ban_chat_member(chat_id, id_to_ban)
    msg = "qua qualcuno ha deciso *spontaneamente* di abbandonarci"
    update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN_V2)


def ping(update, context):
    update.message.reply_text("pong")


def clown(update,context):
        try:
            if(update.message.reply_to_message.from_user.is_bot):
                msg = "*ERRORE*: mai piaciute le catene a me ⛓️⛓️ "
            else:
                msg=update.message.reply_to_message.text
                msg=msg.replace(" "," 🤡 ")
        except:
            msg = "*ERRORE*: devi usare questo comando in risposta a un messaggio"
        update.message.reply_text(msg, parse_mode=ParseMode.MARKDOWN_V2)


def heymoto(update, context):
    update.message.reply_text("https://www.youtube.com/watch?v=fAoT3USbzT0")


def polonia(update, context):
    message = update.message.text
    m = re.search("\s+([^\s]+)", message)
    perchitira = m.group(1)
    if perchitira:
        msg = (f"Ci interrogavamo, per chi sta lavorando la Polonia? \n"
               "Ho la sensazione che stiano lavorando per qualcuno, non e' che stanno lavorando per Sagan?\n"
               f"Lui diceva, perche' e' un ingenuo infondo per {perchitira}, "
               f"ma non mi sembra che siano tutti per {perchitira}\.\n"
               "Sara' la mania dell' indiscreto ma lavorano per qualcun altro\. "
               "Io penso che ci siano degli intrecci tra nazionali e gruppi sportivi\.\n\n"
               "[https://www\.youtube\.com/watch?v\=hWBKHhksGXE](https://www\.youtube\.com/watch?v\=hWBKHhksGXE)")
    else:
        msg = ("*ERRORE*: assicurati di avere usato il comando correttamente\n"
               "ex: `!polonia Kwiatkowski`")
    update.message.reply_text(
        msg, parse_mode=ParseMode.MARKDOWN_V2, disable_web_page_preview=True)


def command_does_not_exist(update, context):
    update.message.reply_text("Questo comando non esiste!")


def new_member(update, context):
    # https://stackoverflow.com/a/62833920
    for member in update.message.new_chat_members:
        if member.first_name:
            identifier = member["first_name"]
        elif member.username:
            identifier = member["username"]
        else:
            identifier = member["id"]
    identifier = escape_markdown(identifier, version=2)
    msg = (f"Benvenuto/a {identifier}\! \n"
           "Prima di continuare assicurati di avere letto il [regolamento](https://t.me/pietreciclismo/10)\. "
           "Buona permanenza\!\n"
           f"[Qui]({LAST_VIDEO_URL}) trovi l' ultimo video su YouTube")
    logging.info(f"{identifier} joined the group")
    update.message.reply_text(
        msg, parse_mode=ParseMode.MARKDOWN_V2, disable_web_page_preview=True)

def debotto(update, context):
    update.message.reply_audio(audio=open('/bot/debotto.mp3', 'rb'))
