import logging
import os

from telegram import Bot
from telegram.ext import CommandHandler, Filters, MessageHandler, Updater
from commands import *

TELEGRAM_TOKEN = os.environ["TELEGRAM_TOKEN"]
bot = Bot(TELEGRAM_TOKEN)

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def main():
    updater = Updater(TELEGRAM_TOKEN)
    dispatcher = updater.dispatcher
    dispatcher.add_handler(MessageHandler(
        Filters.status_update.new_chat_members, new_member))
    dispatcher.add_handler(CommandHandler('help', help_command))
    dispatcher.add_handler(CommandHandler('clown', clown))
    dispatcher.add_handler(CommandHandler('ping', ping))
    dispatcher.add_handler(CommandHandler('heymoto', heymoto))
    dispatcher.add_handler(CommandHandler('polonia', polonia))
    dispatcher.add_handler(CommandHandler('ban', ban))
    dispatcher.add_handler(CommandHandler('debotto', debotto))
    dispatcher.add_handler(MessageHandler(
        Filters.regex(r'^/'), command_does_not_exist))
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
