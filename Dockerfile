FROM docker.io/python:alpine
WORKDIR /bot/
COPY . /bot/
RUN pip3 install -r requirements.txt
CMD ["python3","bot.py"]
