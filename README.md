`PietreBot` is an helper bot for [https://t.me/pietreciclismo](https://t.me/pietreciclismo)

# last video

`https://www.youtube.com/playlist?list=UUO6Q4WydyXJMnRPWQr2cB2g&playnext=1&index=1`

# deploy to fly.io

```bash
flyctl deploy
flyctl secrets set TELEGRAM_TOKEN=<TELEGRAM_TOKEN>
flyctl launch

```